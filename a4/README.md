# LIS4368

## Tyler Martin

### Assignment 4 Requirements:

Create a MVC for Assignment 4 that initiates and validates server-side information.


#### README.md file should include the following items:

##### **Picture of Successful Validation**
##### **Picture of Failed Validation**
##### **Working server-side validation for A4**




#### Assignment Screenshots:

*Screenshot of queryresults*: 

![Screenshot of success](img/failedvalidation.png)
![Screenshot of fail](img/successfulvalidation.png)