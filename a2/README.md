

# LIS4368

## Tyler Martin

### Assignment 2 Requirements:

*Sub-Heading:*

1. Downloaded and installed MySQL
2. Created new user 
3. Developed and Deployed Web Apps
4. Deployed servlet using @WebServlet

#### README.md file should include the following items:

*Pictures of links working*
*Working links of the different localhost directories*
*Screenshot of the query results*

### Working links for directories:

[Hello](http://localhost:9999/hello)

[Index](http://localhost:9999/hello/index.html)

[Sayhello](http://localhost:9999/hello/sayhello)

[Querybook](http://localhost:9999/hello/querybook.html)

[Sayhi](http://localhost:9999/hello/sayhi)

[QueryResult](http://localhost:9999/hello/query?=author=Tan+Ah+Teck)


#### Assignment Screenshots:

*Screenshot of queryresults*: 

![http://localhost:9999/hello/query?author=Tan+Ah+Teck](img/queryresults.png)