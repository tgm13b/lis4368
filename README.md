> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368

## Tyler Martin

### Assignment # Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Create and initialize Bitbucket
    - Git commands

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Download and install MySQL
	- Create new user
	- Develop and deploy WebApp
	- Links properly working
	- Pictures of links properly working

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create petstore database on MySQL Workbench
	- Forward engineer database
	- Take screenshot of petstore database
	- SQL statements

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Compiled the customer and customerlist files 
	- Created the framework for the MVC
	- Completed the server-side validation

5.	[A5 README.md](a5/README.md "My A5 README.md file")
	- Prepared statements to prevent SQL injections
	- JSTL to prevent XSS
	- Added insert functionality to A4


6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Clone student files into repo
	- Adjust index meta tags
	- Create working links for A1, A2, A3, and P1
	- Modify regexpressions for form validation		

7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Valid User entry screenshot
	- Edit data entry screenshot
	- Updated result set screenshot
	- Screenshot of database reflecting new insert
	
