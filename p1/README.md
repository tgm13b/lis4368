# LIS4368

## Tyler Martin

#### README.md file should include the following items:

*Pictures of Main/Splash Page*
*Screenshot of failed validation*
*Screenshot of successful validation*

#### Assignment Screenshots:

*Screenshot of Main/Splash Page*: 

![Screenshot of Main/Splash Page](img/splashpage.png)

*Screenshots of Failed Validaiton*:

![Screenshot of fail1](img/failedvalidation1.png)
![Screenshot of fail2](img/failedvalidation2.png)

*Screenshot of Successful Validation*:

![Screenshot of success](img/successfulvalidation.png)