> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Tyler Martin

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed version control with Git and Bitbucket
2. JAva/JSP/Servlet Development installation
3. Ch questions 1-4

#### README.md file should include the following items:

* Git commands
* JDK installed
* Tomcat installed
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - initiates a git repository
2. git status - shows you statuf of files in the index
3. git add - add files changes to your index
4. git commit - takes all of the changes and commits them
5. git push - pushes all modified local objects to the remote repository
6. git pull - fetches files from the remote repository
7. git rm - removes files from your index and your working directory

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![JDK Installation Screenshot](img/JDKinstalled.png)

*Screenshot of running java Hello*:

![Tomcat Running Screenshot](img/tomcatinstalled.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
