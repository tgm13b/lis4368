# LIS4368

## Tyler Martin

### Assignment 6 Requirements:

Bringing the final project all together. Include client side validation, updating result sets, form functionality on the project 2 page. Must have look-up functionality, search capabality, and delete options.

##README.md file must include 

*Valid User Form Entry screenshot*

*Passed validation screenshot*

*Display data screenshot*

*Modify Form screenshot*

*Modified Data screenshot*

*Associated Database Changes screenshot*

*Assignment Screenshots*

![Screenshot of data](img/data.png)
![Screenshot of delete](img/delete.png)
![Screenshot of display](img/display.png)
![Screenshot of edit](img/edit.png)
![Screenshot of entry](img/entry.png)
![Screenshot of passed](img/passed.png)
![Screenshot of update](img/update.png)