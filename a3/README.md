# LIS4368

## Tyler Martin

### Assignment 3 Requirements:


#### README.md file should include the following items:

*Pictures of A3 ERD*
*SQL file for A3*
*Screenshot of petstore database*



[a3.mwb file](LIS4368_A3.mwb)

[a3.sql file](A3.sql)


#### Assignment Screenshots:

*Screenshot of queryresults*: 

![Screenshot of ERD](img/A3.png)